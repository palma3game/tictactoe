﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
	public bool Occupied = false;
	public Utility.FigureType cellType = Utility.FigureType.None;

	private int rotationCount = 0;
	public GameManager manager;

	void Start()
	{
		manager = GameObject.FindObjectOfType<GameManager>();
	}

	void OnMouseDown()
	{
		if(!Occupied)
		{
			if(manager.CurrentState == GameManager.States.Player1)
			{
				manager.boardController.CellClicked(this, manager.GetCurrentPlayer());
				manager.PlayerMoveState();
			}
		}
	}

	public void OccupyCell(Utility.FigureType type)
	{
		Occupied = true;
		cellType = type;
	}

	public void UnoccupyCell()
	{
		Occupied = false;
		cellType = Utility.FigureType.None;
	}

	#region Cells Interactivity(optional feature)
	public void RotateCell(float speed)
	{
		switch(rotationCount)
		{
		case 0:
			StartCoroutine(RotateLeft(3f,90));
			rotationCount = 1;
			break;
		case 1:
			StartCoroutine(RotateLeft(3f,180));
			rotationCount = 2;
			break;
		case 2:
			StartCoroutine(RotateLeft(3f,270));
			rotationCount = 3;
			break;
		case 3:
			StartCoroutine(RotateUp(3f,90));
			rotationCount = 4;
			break;
		case 4:
			StartCoroutine(RotateUp(3f,270));
			rotationCount = 5;
			break;
		case 5:
			ResetRotation();
			Clear();
			rotationCount = 0;
			break;
		}
	}

	private IEnumerator RotateLeft(float speed, float maxAngle)
	{
		Vector3 rotationDirection = Vector3.up;
		do{
			transform.Rotate(rotationDirection*speed);
			yield return new WaitForEndOfFrame();
		}
		while(transform.eulerAngles.y < maxAngle);
	}
	private IEnumerator RotateUp(float speed, float maxAngle)
	{
		Vector3 rotationDirection = Vector3.forward;
		do{
			transform.Rotate(rotationDirection*speed);
			yield return new WaitForEndOfFrame();
		}
		while(transform.eulerAngles.z < maxAngle);

	}
	private void ResetRotation()
	{
		transform.rotation = new Quaternion();
	}

	public void Clear()
	{
		if(transform.childCount > 0)
		{
			for(int i = 0; i < transform.childCount;i++)
				Destroy(transform.GetChild(i).gameObject);
		}
  	}
	#endregion
}
