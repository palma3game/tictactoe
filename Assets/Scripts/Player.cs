﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {

	private bool isPlayerTurn;
	private string _playersName;

	private Utility.FigureType figureType;
	private GameObject FigurePrefab;

	public string playersName
	{
		get{return _playersName;}
		set{_playersName = value;}
	}

	public Player(string name)
	{
		_playersName = name;
	}

	public bool GetPlayerTurn()
	{
		return isPlayerTurn;
	}
	public void SetPlayerTurn(bool playersTurn)
	{
		isPlayerTurn =  playersTurn;
	}

	public Utility.FigureType GetFigureType()
	{
		return figureType;
	}
	public GameObject GetFigurePrefab()
	{
		return FigurePrefab;
	}

	public void SetFigureType(Utility.FigureType type, GameObject figurePregab)
	{
		FigurePrefab = figurePregab;
		figureType = type;
	}


}
