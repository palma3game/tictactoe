﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicPlayerButton : MonoBehaviour {

	public Text text;
	private MusicPlayer audioPlayer;

	void Start()
	{
		audioPlayer = GameObject.FindObjectOfType<MusicPlayer>();
		text = transform.GetChild(0).GetComponent<Text>();
		audioPlayer.UpdateButtonText(text);
	}

	public void onClick()
	{
		audioPlayer.MusicPlayerButton(text);
	}
}
