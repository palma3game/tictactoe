﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_manager : MonoBehaviour {

	public GameObject GameOverCanvas;
	public GameObject GameplayCanvas;

	public Text winTextObject;
	public Text Player1WinsText;
	public Text DrawsText;
	public Text Player2WinsText;

	public void LoadScene(string SceneName)
	{
		SceneManager.LoadScene(SceneName);
	}
	public void Quit()
	{
		Application.Quit();
	}
	public void NextRound()
	{
		GameObject.FindObjectOfType<GameManager>().MoveNext(GameManager.Events.Restart);
		GameplayCanvas.GetComponent<Canvas>().enabled = true;
		GameOverCanvas.GetComponent<Canvas>().enabled = false;
	}
	public void ShowGameOverCanvas(string winText)
	{
		GameplayCanvas.GetComponent<Canvas>().enabled = false;
		GameOverCanvas.GetComponent<Canvas>().enabled = true;
		winTextObject.text = winText;
	}
	public void WinCounterScreenUpdater(int Player1Wins, int Draws, int Player2Wins)
	{
		Player1WinsText.text = "Player wins :\n"+Player1Wins;
		DrawsText.text = "Draws :\n"+Draws;
		Player2WinsText.text = "Computer wins :\n"+Player2Wins;
	}

	public void mailMe () 
	{
		string email = "vodimed@ukr.net";
		string subject = MyEscapeURL("Test task result");
		string body = MyEscapeURL("Please Enter your message here");
		Application.OpenURL ("mailto:" + email + "?subject=" + subject + "&body=" + body);
	}  
	string MyEscapeURL (string url) 
	{
		return WWW.EscapeURL(url).Replace("+","%20");
	}
}
