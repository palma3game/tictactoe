﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class MusicPlayer : MonoBehaviour {

	static MusicPlayer instance = null;

	public List<AudioClip> gameClipList;

    private AudioSource music;
	private bool AllowPlay = true;
	private AudioClip previousClip;

	void Start () {
		if(instance != null) 
			Destroy(gameObject);
		else
		{
			instance = this;
            DontDestroyOnLoad(gameObject);
			music = GetComponent<AudioSource>();
			NextClip();
        }
	}
	public void Update()
	{
		if(!music.isPlaying && AllowPlay)
			NextClip();
	}

	public void NextClip(){
		previousClip = music.clip;
		if(gameClipList.Count > 1)
		{
			do{
				music.clip = gameClipList[Random.Range(0, gameClipList.Count)];
			} while(previousClip == music.clip);
		}
		music.Play ();
	}
	public void MusicPlayerButton(Text buttonText)
	{
		music.Stop();
		AllowPlay = !AllowPlay;

		foreach(MusicPlayerButton btn in GameObject.FindObjectsOfType<MusicPlayerButton>())
			UpdateButtonText(btn.text);
	}
	public void UpdateButtonText(Text buttonText)
	{
		if(AllowPlay)
			buttonText.text = "Music\nOn";
		else
			buttonText.text = "Music\nOff";
	}
}
