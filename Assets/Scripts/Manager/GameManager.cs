﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility
{
	public enum FigureType {Cross, Dots, None};
}

public class GameManager : MonoBehaviour 
{
	#region VARIABLE DECLARATION
	public GameObject CrossPrefab;
	public GameObject DotPrefab;
	public Cell[] Board = new Cell[9];
	public bool player1Starts = true;

	public BoardController boardController;
	private BoardView boardView;
	private BoardModel boardModel;
	private Player player1;
	private Player player2;
	private UI_manager ui;
	private string GameResultText = "";

	private int Player1WinsCounter = 0;
	private int Player2WinsCounter = 0;
	private int DrawCounter = 0;
	#endregion

	#region STATE MACHINE
	public enum States { StartGame, Player1, Player2, GameOver };
	public States State { get; set; }

	public enum Events { Player1Start, Player2Start, NextPlayer, Win, Draw, Restart };

	class Transitions
	{
		readonly States state;
		readonly Events Event;

		public Transitions(States currentState, Events command)
		{
			state = currentState;
			Event = command;
		}
		public override int GetHashCode()
		{
			return 17 + 31 * state.GetHashCode() + 31 * Event.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			Transitions other = obj as Transitions;
			return other != null && this.state == other.state && this.Event == other.Event;
		}
	}

	Dictionary<Transitions, States> transitions;
	public States CurrentState { get; private set; }

	public GameManager()
	{
		CurrentState = States.StartGame;
		transitions = new Dictionary<Transitions, States>
		{
			{ new Transitions(States.StartGame, Events.Player1Start), States.Player1 },
			{ new Transitions(States.StartGame, Events.Player2Start), States.Player2 },
			{ new Transitions(States.Player1, Events.NextPlayer), States.Player2},
			{ new Transitions(States.Player1, Events.Win), States.GameOver},
			{ new Transitions(States.Player1, Events.Draw), States.GameOver},
			{ new Transitions(States.Player2, Events.Win), States.GameOver},
			{ new Transitions(States.Player2, Events.Draw), States.GameOver},
			{ new Transitions(States.Player2, Events.NextPlayer), States.Player1},
			{ new Transitions(States.GameOver, Events.Restart), States.StartGame }
		};
	}

	public States GetNext(Events command)
	{
		Transitions transition = new Transitions(CurrentState, command);
		States nextState;
		transitions.TryGetValue(transition, out nextState);
		return nextState;
	}

	public States MoveNext(Events command)
	{
		print("Moving from state " + CurrentState + " via " + command + " command." );
		CurrentState = GetNext(command);
		print("Getting to state " + CurrentState);
		CheckState();
		return CurrentState;
	}

	public void CheckState()
	{
		switch(CurrentState) 
		{
		case States.StartGame : 
			StartState();
			break;
		case States.Player1 :
			break;
		case States.Player2 : 
			ComputerMoveState();
			break;
		case States.GameOver : 
			GameOver();
			break;
		}
	}
	#endregion 

	#region State Transition functions

	void StartState()
	{
		print("Start state");
		player1.SetPlayerTurn(player1Starts);
		player2.SetPlayerTurn(!player1Starts);
		print("Player " + GetCurrentPlayer().playersName + "starts ");
		if(player1Starts)
		{
			player1.SetFigureType(Utility.FigureType.Cross,CrossPrefab);
			player2.SetFigureType(Utility.FigureType.Dots,DotPrefab);
			MoveNext(Events.Player1Start);
		}
		else
		{
			player1.SetFigureType(Utility.FigureType.Dots,DotPrefab);
			player2.SetFigureType(Utility.FigureType.Cross,CrossPrefab);
			MoveNext(Events.Player2Start);
		}
	}

	public void PlayerMoveState()
	{
		print("PlayerMove state");
		CheckWin();
	}

	public void ComputerMoveState()
	{
		print("ComputerMove state");
		boardController.ComputerCellClicked(GetCurrentPlayer());
		CheckWin();
	}
	public void GameOver()
	{
		ui.WinCounterScreenUpdater(Player1WinsCounter,DrawCounter,Player2WinsCounter);
		ui.ShowGameOverCanvas(GameResultText);
		boardController.ClearBoard();
	}

	void CheckWin()
	{
		//Checking table for rows, columns and diagonals win cases
		for(int i = 0; i < 3; i++)
		{
			bool checkRowWin = (Board[i*3].cellType == Board[i*3+1].cellType) && (Board[i*3+1].cellType  == Board[i*3+2].cellType) && (Board[i*3].cellType != Utility.FigureType.None);
			bool checkColumnWin = (Board[i].cellType == Board[i+3].cellType) && (Board[i+3].cellType  == Board[i+6].cellType) && (Board[i].cellType != Utility.FigureType.None);
			bool checkDiagonal1Win = (Board[0].cellType == Board[4].cellType) && (Board[4].cellType  == Board[8].cellType) && (Board[0].cellType != Utility.FigureType.None);
			bool checkDiagonal2Win = (Board[2].cellType == Board[4].cellType) && (Board[4].cellType  == Board[6].cellType) && (Board[2].cellType != Utility.FigureType.None);
			//checking if some of the win cases occures
			if(checkRowWin || checkColumnWin || checkDiagonal1Win || checkDiagonal2Win)
			{
				GameResultText = GetCurrentPlayer().playersName + " wins!";
				//Increasing score of the winner
				if(GetCurrentPlayer() == player1)
					Player1WinsCounter++;
				else if(GetCurrentPlayer() == player2)
					Player2WinsCounter++;
				//Choosing who will start next round
				player1Starts = GetCurrentPlayer() == player1;
				// Moving to the next state trough win event
				MoveNext(Events.Win);
				//breaking win function
				return;
			}
		}
		//if win didn't occure - checking draw case
		for(int i = 0; i < Board.Length;i++)
		{
			// if board still has unoccupied fields - then game continues
			if(!Board[i].Occupied)
			{
				// switching players, moving to the next state via next player event and breaking win function
				SwitchPlayer();
				MoveNext(Events.NextPlayer);
				print("Game Continues");
				return;
			}
		}
		// if win didn't occure and board filled completely - declaring draw
		GameResultText = "Draw!";
		//Changing player who starts next round
		player1Starts = !player1Starts;
		DrawCounter++;
		MoveNext(Events.Draw);
		print("Draw");
	}
	#endregion

	void Start()
	{
		ui = GameObject.FindObjectOfType<UI_manager>();
		player1 = new Player("Player");
		player2 = new Player("Computer");
		boardModel = new BoardModel(Board);
		boardView = new BoardView();
		boardController = new BoardController(boardView, boardModel);

		CheckState();
  	}

	#region PlayerManager
	public Player GetCurrentPlayer()
	{
		if(player1.GetPlayerTurn())
			return player1;
		else if(player2.GetPlayerTurn())
			return player2;
		else
			return null;
	}

	public void SwitchPlayer()
	{
		player1.SetPlayerTurn(!player1.GetPlayerTurn());
		player2.SetPlayerTurn(!player2.GetPlayerTurn());
	}
	#endregion
}
