﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardModel
{
	private Cell[] cells;

	public BoardModel(Cell[] board)
	{
		cells = board;
	}

	public void FillCells(Cell[] cells)
	{
		this.cells = cells;
	}

	public Cell[] GetCells()
	{
		return cells;
	}

	public void OccupyCell(Cell cell, Player player)
	{
		cell.OccupyCell(player.GetFigureType());
	}
	public Cell RandomUnoccupiedCell()
	{
		int RandomIndex;
		do
		{
			RandomIndex = Random.Range(0,cells.Length);
		}
		while(cells[RandomIndex].Occupied == true);
		return cells[RandomIndex];
	}

	public void ClearBoardData()
	{
		foreach (Cell cell in cells)
			cell.UnoccupyCell();
	}
}
