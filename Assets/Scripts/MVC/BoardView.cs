﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardView 
{
	public void DisplayOccupiedCell(Cell cell, Player player)
	{
		Debug.Log("cell is " + cell.name + "; player is " + player + "; player figure is " + player.GetFigurePrefab());
		GameObject fig = GameObject.Instantiate(player.GetFigurePrefab(), cell.transform.position+Vector3.back*0.5f, new Quaternion());
		fig.transform.SetParent(cell.transform);
	}

	public void DisplayClearBoard(Cell[] cells)
	{
		foreach (Cell cell in cells)
		{
			cell.RotateCell(3f);
			//cell.Clear();
		}
	}
}
