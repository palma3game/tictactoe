﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardController 
{
	private BoardView boardView;
	private BoardModel boardModel;

	public BoardController(BoardView boardView, BoardModel boardModel)
	{
		this.boardModel = boardModel;
		this.boardView = boardView;
	}

	public void CellClicked(Cell cell, Player player)
	{
		Debug.Log("CellClicked");
		boardModel.OccupyCell(cell,player);
		boardView.DisplayOccupiedCell(cell,player);
	}

	public void ComputerCellClicked(Player player)
	{
		Debug.Log("ComputerCellClicked");
		Cell randomCell = boardModel.RandomUnoccupiedCell();
		boardModel.OccupyCell(randomCell,player);
		boardView.DisplayOccupiedCell(randomCell,player);
	}

	public void ClearBoard()
	{
		boardModel.ClearBoardData();
		boardView.DisplayClearBoard(boardModel.GetCells());
	}
}
